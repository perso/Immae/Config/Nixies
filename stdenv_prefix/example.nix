{ storepath_length ? 42, storepath_chunks ? 6, pkgs ? import <nixpkgs> {
  overlays = [
    (self: super: {
      zstd = super.zstd.overrideAttrs (old: {
        postPatch = old.postPatch + ''
          sed -i 149d build/cmake/lib/CMakeLists.txt
        '';
      });
      glibcLocales = super.glibcLocales.overrideAttrs (old: let
        libc = self.buildPackages.stdenv.cc.libc;
      in {
        buildPhase = ''
          mkdir -vp "$TMPDIR/${libc.new-out}/lib/locale"
        '' + old.buildPhase;
        installPhase = ''
          rmdir $TMPDIR/"${libc.out}/lib/locale"
          ln -s "$TMPDIR/${libc.new-out}/lib/locale" $TMPDIR/"${libc.out}/lib/locale"
        '' + old.installPhase;
      });
    })
  ];
  stdenvStages = { config, overlays, localSystem, ... }@args: (import <nixpkgs/pkgs/stdenv> args) ++ [
    (previousStage: {
      inherit config overlays;
      stdenv = let
        toNewOut = drv: builtins.readFile (pkgs.runCommand "new-out" {} "echo -n $(cat ${drv}/nix-support/new-out) > $out");
        toPreHook = withAppendOut: ''
              storepath_length=${toString storepath_length}
              storepath_chunks=${toString storepath_chunks}
          '' + builtins.readFile ./prehook.sh + (pkgs.lib.optionalString withAppendOut ''
              appendOut
          '');
        newlibunistring = previousStage.libunistring.overrideAttrs (attrs: {
          preHook = attrs.preHook or "" + (toPreHook true);
          am_cv_func_iconv_works = "yes";
        });
        newlibidn2 = (previousStage.libidn2.override { libunistring = newlibunistring; }).overrideAttrs (attrs: {
          preHook = attrs.preHook or "" + (toPreHook true);
          postFixup = ''
            ${previousStage.nukeReferences}/bin/nuke-refs -e $(cat ${newlibunistring}/nix-support/new-out)/lib \
              "$out"/lib/lib*.so.*.*
          '';
        });
        overridenCallPackage = p: a: previousStage.callPackage p (a // { libidn2 = newlibidn2;});

        newlibc = (previousStage."${localSystem.libc}".override { callPackage = overridenCallPackage; }).overrideAttrs(old: {
          preHook = old.preHook or "" + (toPreHook true);
          passthru = { new-out = toNewOut newlibc; };
        });
        newbintools = (previousStage.binutils.override { libc = newlibc; }).overrideAttrs(old: {
          postFixup = old.postFixup + ''
            newlibcout=$(cat ${newlibc}/nix-support/new-out)
            sed -i -e "s@${newlibc}@$newlibcout@g" $out/nix-support/*
          '';
        });
        newStdenv = previousStage.stdenv.override {
          allowedRequisites = null;
          extraAttrs.bintools = newbintools;
          cc = previousStage.gcc.override({ bintools = newbintools; libc = newlibc; });
          overrides = self: super: rewriteMap (previousStage.stdenv.overrides self super) // {
            bash = super.bash.overrideAttrs (old: { passthru = old.passthru // { new-out = toNewOut super.bash; }; });
            ${localSystem.libc} = newlibc;
          };
          preHook = previousStage.stdenv.preHook + ''
              libc_path=${newlibc}
            '' + (toPreHook false);
        };
        rewriteMap = builtins.mapAttrs (n: v: v.override { stdenv = newStdenv; });
      in
        newStdenv;
      })
      (previousStage: {
        inherit config overlays;
        stdenv = previousStage.stdenv.override {
          shell = previousStage.bash + "/bin/bash";
          initialPath = ((import <nixpkgs/pkgs/stdenv/common-path.nix>) {pkgs = previousStage;});
        };
      })
    ];
} }:
  pkgs
