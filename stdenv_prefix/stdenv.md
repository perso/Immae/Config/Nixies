Ce dossier contient un travail permettant de créer des dérivations qui
vont toutes commencer par un préfix de taille et avec un nombre de
composants donné (de la forme x/x/xxxxx)

Le but étant de pouvoir copier une closure d’une dérivation, la mettre
par exemple dans /opt/nix/store/....-foo et avec un simple `rename` +
`sed` la rendre utilisable (dans l’exemple /nix/store/....-foo/xxx ->
/opt/nix/store/....-foo).

Ci dessous se trouvent des notes de ce que j’ai compris de la stack
stdenv (pkgs/stdenv/linux/default.nix)

# Stage -1
  gcc-unwrapped = null
  binutils = null
  coreutils = null
  gnugrep = null
  `stagem1 = stdenv.__bootPackages.stdenv.__bootPackages.stdenv.__bootPackages.stdenv.__bootPackages.stdenv.__bootPackages.stdenv.__bootPackages`
  -> contient cinq entrées (dont stdenv "invalide")

# Stage 0
  `stage0 = stdenv.__bootPackages.stdenv.__bootPackages.stdenv.__bootPackages.stdenv.__bootPackages.stdenv.__bootPackages`
  (`stagem1 = stage0.stdenv.__bootPackages`)
  `stage0.stdenv`
  -> cc = null
  `stage0`
  overrides:
    - glibc = extrait (ln -s) de bootstrapTools (bootstrap-stage0-glibc)
    - gcc-unwrapped = bootstrapTools
    - binutils = wrapper sur bootstrapTools (bootstrap-stage0-binutils-wrapper)
    - coreutils = bootstrapTools
    - gnugrep = bootstrapTools
    - ccWrapperStdenv = stage0.stdenv

# Stage 1
  `stage1 = stdenv.__bootPackages.stdenv.__bootPackages.stdenv.__bootPackages.stdenv.__bootPackages`
  (`stage0 = stage1.stdenv.__bootPackages`)
  `stage1.stdenv`
  -> cc = cc-wrapper buildé avec
            cc = stage0.gcc-unwrapped = bootstrapTools
            bintools = stage0.binutils = bootstrap-stage0-binutils-wrapper
            glibc = stage0.glibc = bootstrap-stage0-glibc
            coreutils = stage0.coreutils = bootstrapTools
            gnugrep = stage0.gnugrep = bootstrapTools
            stdenvNoCC = stage0.ccWrapperStdenv = stage0.stdenv
  `stage1`
  overrides:
    - binutils-unwrapped = override (gold = false)
    - ccWrapperStdenv = stage0.stdenv
    - gcc-unwrapped = bootstrapTools
    - coreutils = bootstrapTools
    - gnugrep = bootstrapTools
    - glibc = bootstrap-stage0-glibc
    - perl = override (threading = false)

# Stage 2
  `stage2 = stdenv.__bootPackages.stdenv.__bootPackages.stdenv.__bootPackages`
  (`stage1 = stage2.stdenv.__bootPackages`)
  `stage2.stdenv`
  -> cc = cc-wrapper buildé avec
          cc = stage1.gcc-unwrapped = bootstrapTools
          bintools = stage1.binutils = wrapper sur binutils-unwrapped (gold = false)
          glibc = bootstrap-stage0-glibc
          coreutils = bootstrapTools
          gnugrep = bootstrapTools
          stdenvNoCC = stage1.ccWrapperStdenv = stage0.stdenv
  `stage2`
  overrides:
    - ccWrapperStdenv = stage0.stdenv
    - gcc-unwrapped = bootstrapTools
    - coreutils = bootstrapTools
    - gnugrep = bootstrapTools
    - perl = override (threading = false)
    - binutils = stage1.override (libc = self.glibc)
    - libidn2 = nuke-refs
    - libunistring = nuke-refs
    - dejagnu = nocheck
    - gnum4 = stage1.gnum4
    - bison = stage1.bison

# Stage 3
  `stage3 = stdenv.__bootPackages.stdenv.__bootPackages`
  (`stage2 = stage3.stdenv.__bootPackages`)
  `stage3.stdenv`
  -> cc = cc-wrapper buildé avec
          cc = stage2.gcc-unwrapped = bootstrapTools
          bintools = stage2.binutils
          glibc = stage2.glibc
          coreutils = bootstrapTools
          gnugrep = bootstrapTools
          stdenvNoCC = stage2.ccWrapperStdenv = stage0.stdenv
  `stage3`
  overrides:
    - ccWrapperStdenv = stage0.stdenv
    - binutils = stage2.binutils
    - coreutils = bootstrapTools
    - gnugrep = bootstrapTools
    - perl patchelf linuxHeaders gnum4 bison libidn2 libunistring = stage2
    - glibc = stage2.glibc
    - gmp mpfr libmpc isl_0_20 = override static
    - gcc-unwrapped = override (isl)

# Stage 4
  `stage4 = stdenv.__bootPackages`
  (`stage3 = stage4.stdenv.__bootPackages`)
  `stage4.stdenv`
  -> cc = cc-wrapper buildé avec
          cc = stage3.gcc-unwrapped = override (isl)
          bintools = stage2.binutils
          glibc = stage2.glibc
          coreutils = bootstrapTools
          gnugrep = bootstrapTools
          stdenvNoCC = stage3.ccWrapperStdenv = stage0.stdenv
  `stage4`
  overrides:
    - gettext gnum4 bison gmp perl texinfo zlib linuxHeaders libidn2 libunistring = stage3
    - glibc = stage3.glibc = stage2.glibc
    - binutils = override
    - gcc = buildé avec
            cc = stage3.gcc-unwrapped
            bintools = self.binutils
            libc = self.glibc
            stdenvNoCC coreutils gnugrep = self

# Final Stage
  `stdenv`
  cc = stage4.gcc
  `pkgs`
  overrides:
    - glibc = stage4.glibc = stage2.glibc
    - zlib libidn2 libunistring = stage4 = stage3
    - (hors cross-compile) binutils binutils-unwrapped = stage4
    - (hors cross-compile) gcc = cc
