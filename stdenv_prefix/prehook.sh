count_chunks() {
  elements="${1//[^\/]}"
  echo $(( "${#elements}" ))
}

oldout=$out
prefix_chunks=$(count_chunks $NIX_STORE)

: ${storepath_length=${#NIX_STORE}}
: ${storepath_chunks=$prefix_chunks}

if [ "$storepath_chunks" -lt $prefix_chunks ]; then
  echo "Need to have at least as much path elements as the initial prefix"
  exit 1
fi

min_length=$(( ${#NIX_STORE} + 2 * $storepath_chunks - 2 * $prefix_chunks ))
if [ "$storepath_length" -lt "$min_length" ]; then
  echo "The storepath length needs to be at least the length of the prefix"
  exit 1
fi

if [ "$storepath_chunks" -eq "$prefix_chunks" -a "$storepath_length" -gt "$min_length" ]; then
  echo "You need at least one more chunk for a non-standard path length"
  exit 1
fi

appendOut() {
  chunk_prefix=$(for (( c=$prefix_chunks ; c < $storepath_chunks ; c++ )); do echo -n "/x"; done)
  length_prefix=$(for (( c=$(( ${#NIX_STORE} + ${#chunk_prefix} )) ; c < $storepath_length ; c++ )); do echo -n "x"; done)
  out="$out$chunk_prefix$length_prefix"
  if [ -n $chunk_prefix -o -n $length_prefix ]; then
    mkdir -p $(dirname $out)
  fi

  : ${libc_path=}
  : ${configureFlags=}
  : ${installFlags=}
  installFlags="${installFlags//$oldout/$out}"
  configureFlags="${configureFlags//$oldout/$out}"
  if [ -n "$libc_path" ]; then
    libc_oldpath=$libc_path/lib
    libc_newpath=$(cat $libc_path/nix-support/new-out)/lib
    configureFlags="${configureFlags//$libc_oldpath/$libc_newpath}"
    installFlags="${installFlags//$libc_oldpath/$libc_newpath}"
  fi
}

addNixSupport() {
  if [ ! -d $out ]; then
    return
  fi
  mkdir -p $out/nix-support
  echo $out > $out/nix-support/new-out
}

overridePlaceholder() {
function _callImplicitHook {
  local def="$1"
  local hookName="$2"
  if declare -F "$hookName" > /dev/null; then
      "$hookName"
  elif type -p "$hookName" > /dev/null; then
      source "$hookName"
  elif [ -n "${!hookName:-}" ]; then
      content="${!hookName}"
      eval "${content//$oldout/$out}"
  else
      return "$def"
  fi
}
}

preHooks+=(appendOut overridePlaceholder)

restoreSingleFile() {
  if [ "$out" != "$oldout" -a -f $out ]; then
    cp -a $out ./file
    rm -rf $oldout
    cp -a ./file $oldout
  fi
}

addSymlinks() {
  if [ "$out" != "$oldout" -a -d $out ]; then
    ln -s $out/* $oldout/
  fi
}

rename_generic_build() {
eval "$(echo "orig_genericBuild()"; declare -f genericBuild | tail -n +2)"
function genericBuild {
  orig_genericBuild "$@"
  restoreSingleFile
  addSymlinks
  addNixSupport
}
}

exitHooks+=(restoreSingleFile addSymlinks addNixSupport)
postHooks+=(rename_generic_build)
