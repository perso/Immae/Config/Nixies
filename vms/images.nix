let
  pkgs = import <nixpkgs> {};
  lib = pkgs.lib;
  toEval = modules:
    import <nixpkgs/nixos/lib/eval-config.nix> {
      system = pkgs.system;
      modules = [ ./configuration.nix ] ++ modules;
    };
  modules = {
    docker = [
      {
        config = {
          boot.isContainer = true;
          system.activationScripts.installInitScript = ''
            ln -fs $systemConfig/init /init
          '';
        };
      }
    ];
    light = [
      { config.virtualisation.graphics = false; }
      <nixpkgs/nixos/modules/virtualisation/qemu-vm.nix>
    ];
    standalone = [
      {
        config = {
          fileSystems."/" = {
            device = "/dev/disk/by-label/nixos";
            fsType = "ext4";
            autoResize = true;
          };

          boot = {
            kernelParams = [ "console=ttyS0" ];
            loader = {
              timeout = 0;
              grub.device = "/dev/xvda";
              grub.configurationLimit = 0;
            };

            initrd = {
              network.enable = true;
            };
          };

          services.udisks2.enable = false;
        };
      }
    ];
  };
  evals = {
    light = (toEval modules.light).config.system.build.vm;
    docker =
      # inspired from
      # https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/virtualisation/docker-image.nix
      let
        eval = toEval modules.docker;
      in
        pkgs.callPackage <nixpkgs/nixos/lib/make-system-tarball.nix> {
          contents = [
            {
              source = "${eval.config.system.build.toplevel}/.";
              target = "./";
            }
          ];
          extraArgs = "--owner=0";

          # Add init script to image
          storeContents = map (x: { object = x; symlink = "none"; }) [
            eval.config.system.build.toplevel
            pkgs.stdenv
          ];

          # Some container managers like lxc need these
          extraCommands = "mkdir -p proc sys dev";
        };
    standalone =
      let
        eval = toEval modules.standalone;
        name = "nixos-${eval.config.system.nixos.label}-${pkgs.stdenv.hostPlatform.system}";
      in
        import <nixpkgs/nixos/lib/make-disk-image.nix> {
          inherit lib name pkgs;
          config = eval.config;
          contents = [];
          diskSize = 2048;
          format = "qcow2";
          postVM = ''
            extension=''${diskImage##*.}
            friendlyName=$out/${name}.$extension
            mv "$diskImage" "$friendlyName"
            diskImage=$friendlyName

            mkdir -p $out/nix-support

            ${pkgs.jq}/bin/jq -n \
              --arg label ${lib.escapeShellArg eval.config.system.nixos.label} \
              --arg system ${lib.escapeShellArg pkgs.stdenv.hostPlatform.system} \
              --arg logical_bytes "$(${pkgs.qemu}/bin/qemu-img info --output json "$diskImage" | ${pkgs.jq}/bin/jq '."virtual-size"')" \
              --arg file "$diskImage" \
              '$ARGS.named' \
              > $out/nix-support/image-info.json
          '';
        };
  };
  scripts = {
    standalone = pkgs.writeScript "run" ''
      #!${pkgs.stdenv.shell}

      file=$(cat ${evals.standalone}/nix-support/image-info.json | jq -r .file)
      cp $file ./nixos.qcow2
      chmod u+w nixos.qcow2

      trap "rm -f nixos.qcow2" EXIT
      ${pkgs.qemu}/bin/qemu-system-x86_64 -nographic --cpu host --enable-kvm -hda nixos.qcow2
    '';
    light = pkgs.writeScript "run" ''
      #!${pkgs.stdenv.shell}

      trap "rm -f nixos.qcow2" EXIT
      ${evals.light}/bin/run-nixos-vm
    '';
    docker = pkgs.writeScript "run" ''
      #!${pkgs.stdenv.shell}

      docker import ${evals.docker}/tarball/nixos-system-*.tar.xz nixos-docker
      cid=$(docker run --rm --privileged --detach nixos-docker /init)
      trap "docker stop $cid" EXIT
      sleep 10
      docker exec -it $cid /run/current-system/sw/bin/bash
    '';
  };
in
  lib.mapAttrs (name: v: v // { run = scripts.${name}; eval = evals.${name}; modules = modules.${name};}) scripts
