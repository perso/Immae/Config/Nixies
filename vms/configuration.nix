{ pkgs, ... }:
{
  config = {
    users.users.root.password = "";
    users.mutableUsers = false;

    environment.systemPackages = [
      pkgs.curl
    ];
    systemd.services.django-hello-world = {
      description = "An example django app";
      wantedBy = [ "multi-user.target" ];
      after    = [ "network.target" ];

      preStart = "rm -rf /var/lib/django_app/test_app && cp -a ${./test_django} /var/lib/django_app/test_app";
      script =
        let pythonWithDjango = pkgs.python3.withPackages (p: [ p.django ]);
        in "cd /var/lib/django_app/test_app && ${pythonWithDjango}/bin/python manage.py runserver";
      serviceConfig = {
        WorkingDirectory = "/var/lib/django_app";
        StateDirectory = "django_app";
      };
    };
  };
}
