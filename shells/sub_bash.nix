# run with
# nix-shell shell.nix --run 'touch in ; tail -f in | bash & echo $! > pid'
# and then send commands with
# echo 'compute_me "1 + 3"' >> in
{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  buildInputs = [ pkgs.bc ];
  shellHook = ''
    compute_me() {
      echo "$1" | bc;
    }
    export -f compute_me

    echo "I’m ready"
    '';
}
