with import <nixpkgs> {};
  mkShell {
    buildInputs = [ postgresql_11 glibcLocales ];
    shellHook = ''
            export PGDB="dummy";
            export PGDATA=$PWD/postgres
            export PGHOST=$PWD/postgres
            export PGPORT=5432
            export LOG_PATH=$PWD/postgres/LOG
            export PGDATABASE=postgres
            export DATABASE_URL="postgresql:///postgres?host=$PGDATA"
            export LANG=en_US.UTF-8;
            export LOCALE_ARCHIVE=${glibcLocales}/lib/locale/locale-archive;
            mkdir -p $PGDATA
            echo 'Initializing postgresql database...'
            initdb $PGDATA --auth=trust >/dev/null
            pg_ctl start -w -l $LOG_PATH -o "-c synchronous_commit=off -c listen_addresses= -c unix_socket_directories=$PGDATA"
            createdb "$PGDB";
            finish() {
                pg_ctl stop -m fast;
                rm -rf "$PGDATA";
            }
            trap finish EXIT
    '';
  }

