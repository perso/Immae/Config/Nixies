This directory regroups regularly-accessed nix-shells that require (or
not) a bit of configuration to run and may not be trivial to write from
scratch.
