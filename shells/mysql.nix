with import <nixpkgs> {};
  mkShell {
    buildInputs = [ mariadb ];
    shellHook = ''
      export MARIADBHOST=$PWD/mysql
      export LANG=en_US.UTF-8;
      export LOCALE_ARCHIVE=${glibcLocales}/lib/locale/locale-archive;
      export MYSQL_UNIX_PORT=$MARIADBHOST/mysql.sock
      mkdir -p $MARIADBHOST
      cat > $MARIADBHOST/my.cnf <<EOF
      [mysqld]
      skip-networking
      datadir=$MARIADBHOST
      socket=$MARIADBHOST/mysql.sock
      EOF
      echo 'Initializing mysql database...'
      mysql_install_db --defaults-file=$MARIADBHOST/my.cnf --datadir=$MARIADBHOST --basedir=${mariadb} > $MARIADBHOST/LOG 2>&1
      mysqld --defaults-file=$MARIADBHOST/my.cnf --datadir=$MARIADBHOST --basedir=${mariadb} --pid-file=$MARIADBHOST/mariadb.pid >> $MARIADBHOST/LOG 2>&1 &
      finish() {
          mysqladmin shutdown
          rm -rf "$MARIADBHOST";
      }
      trap finish EXIT
    '';
  }

